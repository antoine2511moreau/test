class TestClass:
    def test_one(self):
        x = "this"
        assert "h" in x

    def test_two(self):
        # mock
        class X:
          check = "toto"
        x = X()        
        assert hasattr(x, "check")
