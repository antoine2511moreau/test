.. testdoc documentation master file, created by
   sphinx-quickstart on Wed Dec 14 11:14:31 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=================
     Bonjour 
=================

Bienvenue dans l'univers du sanglier
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. image:: logo.png

Terrine
============

.. index:: contenu

Terrine de sanglier 100% francais

Recette secrette

.. glossary::

   Accompagnement

       Avec votre terrine nous vous recommendons un bon vin rouge de type beaujolais

   Pain

       Le choix du pain est important également ! Pensez pain de campagne !

   Copains

       La terrine aura meilleure gout si elle est accompagnée de vos meilleurs copains



Gigot
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
